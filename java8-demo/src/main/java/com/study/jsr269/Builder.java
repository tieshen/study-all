package com.study.jsr269;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.SOURCE)
@Documented
@Inherited
public @interface Builder {



}
