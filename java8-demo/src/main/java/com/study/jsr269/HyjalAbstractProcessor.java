package com.study.jsr269;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * RetentionPolicy.SOURCE
 */
@SupportedAnnotationTypes(value = {"com.study.jsr269.Hyjal"})
@SupportedSourceVersion(value = SourceVersion.RELEASE_8)
public class HyjalAbstractProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        System.out.println("Log in AnnotationProcessor.process");
        for (TypeElement annotation : annotations) {
            System.out.println(annotation);
        }

        System.out.println(roundEnv);
        return true;
    }

}
