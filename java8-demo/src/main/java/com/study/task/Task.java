package com.study.task;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 一个任务
 */
public class Task {

    private String id;

    private boolean result = false;

    private UploadFile uploadFile;

    public Task(UploadFile uploadFile) {
        this.uploadFile = uploadFile;
        this.id = uploadFile.getTaskId().toString();
    }

    /**
     * 来进行文件的转移操作 IO
     */
    public Future<Boolean> move(UploadFile uploadFile) {
        return CompletableFuture.supplyAsync(() -> doMove(uploadFile));
    }

    private boolean doMove(UploadFile uploadFile) {
        delay();
        // start-copy
        return true;
    }

    /**
     * 模拟延迟
     */
    public static void delay() {
        try {
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public UploadFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(UploadFile uploadFile) {
        this.uploadFile = uploadFile;
    }

}
