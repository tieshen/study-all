package com.study.task;

import java.util.List;
import java.util.Map;

/**
 * 数据库查询所有的数据
 */
public interface QueryData {

    /**
     * 查询任务
     *
     * @param taskGroup
     * @return
     */
    List<Task> query(TaskGroup taskGroup);

    /**
     * 查询任务
     *
     * @param taskContainer
     * @return
     */
    Map<String, List<Task>> query(TaskContainer taskContainer);

}
