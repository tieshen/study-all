package com.study.task;

import java.util.concurrent.CompletableFuture;

public class Demo {

    public static void main(String[] args) {
//        String result = CompletableFuture.supplyAsync(Demo::get).thenApplyAsync(v -> v + "world").join();
//        System.out.println(result);

        TaskContainer taskContainer = new TaskContainer(1722);
        System.out.println("start");
        taskContainer.run();
    }

    private static String get() {
        return "Hello ";
    }

}
