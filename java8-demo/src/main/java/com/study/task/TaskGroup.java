package com.study.task;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 一组任务
 */
public class TaskGroup {

    private String id;

    private UploadMapper uploadMapper = new UploadMapper();

    private Boolean result = false;

    private int startIndex;

    private int endIndex;

    private Map<String, Task> taskMap;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Task> getTaskMap() {
        return taskMap;
    }

    public void setTaskMap(Map<String, Task> taskMap) {
        this.taskMap = taskMap;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public TaskGroup queryData() {
        List<UploadFile> query = uploadMapper.query(this);
        this.taskMap = query.stream()
                .map(uploadFile -> new Task(uploadFile))
                .collect(Collectors.toMap(Task::getId, task -> task));
        return this;
    }

}
