package com.study.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UploadMapper {

    public List<UploadFile> query(TaskGroup taskGroup) {
        List<UploadFile> uploadFiles = new ArrayList<>();
        for (int i = taskGroup.getStartIndex(); i < taskGroup.getEndIndex(); i++) {
            UploadFile uploadFile = new UploadFile();
            uploadFile.setFileId((long) i);
            uploadFile.setTaskId(String.valueOf(i));
            uploadFile.setFileName(String.valueOf(i));
            uploadFiles.add(uploadFile);
            deley();
        }

        return uploadFiles;
    }

    private void deley() {
        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("error");
            System.out.println(e);
        }
    }

}
