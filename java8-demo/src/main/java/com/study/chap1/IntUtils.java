package com.study.chap1;

public final class IntUtils {

    /**
     * 是否是偶数
     *
     * @param i
     * @return
     */
    public static boolean even(int i) {
        int i1 = i & 1;
        return i1 == 1 ? false : true;
    }

    /**
     * 是否是奇数
     *
     * @param i
     * @return
     */
    public static boolean odd(int i) {
        return !even(i);
    }

    public static void main(String[] args) {
        System.out.println(even(10));
        System.out.println(even(1));
        System.out.println(even(3));
        System.out.println(even(11));
        System.out.println(even(42));
    }

}
