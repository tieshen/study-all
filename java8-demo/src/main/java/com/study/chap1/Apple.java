package com.study.chap1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Apple implements Serializable {

    private String color;

    private int weight;

    public Apple(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return StringUtils.toString(Apple.class, "color", color, "weight", weight);
    }

    static List<Apple> filterApples(List<Apple> inventory, Predicate<Apple> p) {
        List<Apple> result = new ArrayList<>();

        for (Apple apple : inventory) {
            if (p.test(apple)) {
                result.add(apple);
            }
        }

        return result;
    }

    public static boolean isGreenApple(Apple apple) {
        return "green".equalsIgnoreCase(apple.getColor());
    }

    public static boolean isHeavyApple(Apple apple) {
        return apple.getWeight() > 150;
    }

    public static void main(String[] args) {
        List<Apple> apples = new ArrayList<>();
        apples.add(new Apple("green", 110));
        apples.add(new Apple("yellow", 130));
        apples.add(new Apple("green", 170));
        apples.add(new Apple("blue", 155));
        List<Apple> result = filterApples(apples, Apple::isGreenApple);
        System.out.println("绿色的苹果");
        result.stream().forEach(System.out::println);
        result = filterApples(apples, (apple) -> "yellow".equalsIgnoreCase(apple.getColor()));
        System.out.println("黄色的苹果");
        result.stream().forEach(System.out::println);
    }

}
