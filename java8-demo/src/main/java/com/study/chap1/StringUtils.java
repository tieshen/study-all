package com.study.chap1;

/**
 * @date: 2019-03-31 10:56
 * @author: ironc
 * @version: 1.0
 */
public final class StringUtils {

    /**
     * {@link System#out}
     *
     * @param clazz
     * @param args
     * @return
     */
    public static String toString(Class clazz, Object... args) {
        StringBuilder sb = new StringBuilder();
        sb.append(clazz.getName())
                .append("{");
        int length = args.length;
        boolean even = IntUtils.even(length);
        assert even : "参数组合不是偶数，请校验参数";
        for (int i = 0; i < length; i++) {
            sb.append(args[i]);
            if (IntUtils.even(i)) {
                sb.append("=");
            } else {
                sb.append(";");
            }
        }
        sb.append("}");

        return sb.toString();
    }


}
