package com.study.not8.annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Map;

/**
 * @author tc
 * @date 2019/2/13
 */
public class DemoAnnotationRun {

    private static DemoAnnotation demoAnnotation;

    public static void main(String[] args)
        throws IllegalAccessException, InstantiationException, NoSuchFieldException, ClassNotFoundException {
        ClassLoader classLoader = DemoAnnotation.class.getClassLoader();

        // error interface can not
        Class<?> demoAnnotation = classLoader.loadClass("DemoAnnotation");

        DemoAnnotationRun.demoAnnotation = DemoAnnotation.class.newInstance();

        InvocationHandler h = Proxy.getInvocationHandler(DemoAnnotationRun.demoAnnotation);

        Field hField = h.getClass().getDeclaredField("memberValues");

        hField.setAccessible(true);

        Map memberValues = (Map)hField.get(h);

        memberValues.put("name", "tiecheng");

        System.out.printf("name is : ", DemoAnnotationRun.demoAnnotation.name());
    }

}
