package com.study.not8.annotation;

import java.lang.annotation.*;

/**
 * @author tc
 * @date 2019/2/13
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DemoAnnotation {

    String name() default "";

}
