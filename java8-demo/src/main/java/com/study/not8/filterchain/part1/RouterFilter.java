package com.study.not8.filterchain.part1;

/**
 * @author tc
 * @date 2018/12/18
 */
public class RouterFilter implements Filter {

    @Override
    public Object getData(FilterChainWrapper chain) {
        System.out.println("筛选路由");
        return chain.getData(chain);
    }

}
