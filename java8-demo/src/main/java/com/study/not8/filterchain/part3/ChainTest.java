package com.study.not8.filterchain.part3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author tc
 * @date 2018/12/18
 */
public class ChainTest {

    private static List<FilterWithChainAndLambda> oneChainWithLambdaFilter;

    private static List<FilterWithChainAndLambda> fiveChainWithLambdaFilter;

    static {
        oneChainWithLambdaFilter = new ArrayList<>();
        oneChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());

        fiveChainWithLambdaFilter = new ArrayList<>();
        fiveChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());
        fiveChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());
        fiveChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());
        fiveChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());
        fiveChainWithLambdaFilter.add(new FilterWithChainAndLambdaImpl());
    }

    //@Benchmark
    //@BenchmarkMode(Mode.Throughput)
    //@OutputTimeUnit(TimeUnit.MILLISECONDS)
    //@Threads(value = 5)
    //@Warmup(iterations = 2, time = 1)
    //@Measurement(iterations = 5, time = 1)
    //@Fork(value = 2)
    public void withFiveChainAndLambda(){
        FilterChainAndLambdaWrapper chain = new FilterChainAndLambdaWrapper(fiveChainWithLambdaFilter);
        chain.doFilter(() -> originMethod(), chain);
    }

    private static void originMethod() {
    }
}
