package com.study.not8.filterchain.part3;

/**
 * @author tc
 * @date 2018/12/18
 */
public interface FilterWithChainAndLambda {

    void doFilter(FilterChainFunctionalInterface action, FilterChainAndLambdaWrapper chain);

}
