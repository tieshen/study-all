package com.study.not8.filterchain.part3;

/**
 * @author tc
 * @date 2018/12/18
 */
@FunctionalInterface
public interface FilterChainFunctionalInterface {

    void doFilter();

}
