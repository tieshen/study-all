package com.study.not8.filterchain.part3;

import java.util.List;

/**
 * @author tc
 * @date 2018/12/18
 */
public class FilterChainAndLambdaWrapper implements FilterWithChainAndLambda {

    private final List<FilterWithChainAndLambda> filters;

    public int index = 0;

    public FilterChainAndLambdaWrapper(List<FilterWithChainAndLambda> filters) {
        this.filters = filters;
    }

    @Override
    public void doFilter(FilterChainFunctionalInterface action, FilterChainAndLambdaWrapper chain) {
        if (index < filters.size()) {
            filters.get(index++).doFilter(action, chain);
        } else {
            action.doFilter();
        }
    }
}
