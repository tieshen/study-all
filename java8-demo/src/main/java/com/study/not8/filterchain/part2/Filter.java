package com.study.not8.filterchain.part2;

/**
 * @author tc
 * @date 2018/12/18
 */
public interface Filter {

    void doFilter(Request request, Response response, FilterChain chain);

}
