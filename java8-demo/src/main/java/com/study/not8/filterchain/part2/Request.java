package com.study.not8.filterchain.part2;

/**
 * @author tc
 * @date 2018/12/18
 */
public class Request {

    private String requestStr;

    public Request() {
    }

    public Request(String requestStr) {
        this.requestStr = requestStr;
    }

    public String getRequestStr() {
        return requestStr;
    }

    public void setRequestStr(String requestStr) {
        this.requestStr = requestStr;
    }

}
