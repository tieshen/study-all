package com.study.not8.filterchain.part1;

import java.util.ArrayList;
import java.util.List;

/**
 * 静态责任链 修改比较麻烦
 * @author tc
 * @date 2018/12/18
 */
public class ChainDemo01 {

    private static List<Filter> filters;


    static {
        filters = new ArrayList<>();
        filters.add(new LogFilter());
        filters.add(new RouterFilter());
    }

    public static void main(String[] args) {
        FilterChainWrapper filterChainWrapper = new FilterChainWrapper(filters);
        filterChainWrapper.getData(filterChainWrapper);
    }

}
