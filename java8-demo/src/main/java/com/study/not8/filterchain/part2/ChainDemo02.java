package com.study.not8.filterchain.part2;

/**
 * @author tc
 * @date 2018/12/18
 */
public class ChainDemo02 {

    public static void main(String[] args) {
        FilterChain filterChain = new FilterChain();
        filterChain.addFilter(new LogFilter());
        filterChain.addFilter(new RouterFilter());
        filterChain.doFilter(new Request("request"), new Response("response"), filterChain);
    }

}
