package com.study.not8.filterchain.part2;

/**
 * @author tc
 * @date 2018/12/18
 */
public class RouterFilter implements Filter {

    @Override
    public void doFilter(Request request, Response response, FilterChain chain) {

        System.out.println("router-req");

        chain.doFilter(request, response, chain);

        System.out.println("router-res");
    }

}
