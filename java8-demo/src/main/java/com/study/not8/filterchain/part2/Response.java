package com.study.not8.filterchain.part2;

/**
 * @author tc
 * @date 2018/12/18
 */
public class Response {

    private String responseStr;

    public Response() {
    }

    public Response(String responseStr) {
        this.responseStr = responseStr;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

}
