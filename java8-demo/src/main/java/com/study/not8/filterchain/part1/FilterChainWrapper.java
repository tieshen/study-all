package com.study.not8.filterchain.part1;

import java.util.List;

/**
 * @author tc
 * @date 2018/12/18
 */
public class FilterChainWrapper implements Filter {

    private final List<Filter> filterList;

    public FilterChainWrapper(List<Filter> filterList) {
        this.filterList = filterList;
    }

    public int index = 0;

    @Override
    public Object getData(FilterChainWrapper chain) {
        if (index < filterList.size()) {
            return filterList.get(index++).getData(chain);
        } else {
            return null;
        }
    }
}
