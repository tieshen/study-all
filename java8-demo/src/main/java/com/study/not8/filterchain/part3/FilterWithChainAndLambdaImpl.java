package com.study.not8.filterchain.part3;

/**
 * @author tc
 * @date 2018/12/18
 */
public class FilterWithChainAndLambdaImpl implements FilterWithChainAndLambda {

    @Override
    public void doFilter(FilterChainFunctionalInterface action, FilterChainAndLambdaWrapper chain) {
        chain.doFilter(action, chain);
    }

}
