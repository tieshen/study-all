package com.study.not8.filterchain.part1;

/**
 * @author tc
 * @date 2018/12/18
 */
public interface Filter {

    Object getData(FilterChainWrapper chain);

}
