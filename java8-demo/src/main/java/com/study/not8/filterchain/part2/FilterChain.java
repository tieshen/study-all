package com.study.not8.filterchain.part2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tc
 * @date 2018/12/18
 */
public class FilterChain implements Filter {

    private List<Filter> filters = new ArrayList<>();

    private int index = 0;

    public FilterChain addFilter(Filter filter) {
        this.filters.add(filter);
        return this;
    }

    @Override
    public void doFilter(Request request, Response response, FilterChain chain) {
        if (index >= filters.size()) {
            return;
        }
        Filter f = filters.get(index);
        index++;
        f.doFilter(request, response, chain);
    }

}
