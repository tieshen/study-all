package com.study.not8.algorithm;

/**
 * 迭代
 * @author tc
 * @date 2018/12/18
 */
public class RecursiveDemo {

    public static void main(String[] args) {
        int k = 4;
        Result result = new Result();
        boolean prove = Lesson4_2.prove(k, result);

        System.out.println("num " + result.num + " totalNum " + result.totalNum);
        System.out.println(prove);
    }

    static class Result {
        public long num = 0;

        public long totalNum = 0;
    }

    static class Lesson4_2 {

         static boolean prove(int k, Result result) {
            if (k == 1) {
                if (Math.pow(2, 1) - 1 == 1) {
                    result.num = 1;
                    result.totalNum = 1;
                    return true;
                } else {
                    return false;
                }
            } else {
                boolean proveOfPreviousOne = prove(k - 1, result);
                result.num *= 2;
                result.totalNum += result.num;
                boolean proveOfCurrentOne = false;
                if (result.totalNum == (Math.pow(2, k) - 1)) {
                    proveOfCurrentOne = true;
                }

                if (proveOfPreviousOne && proveOfCurrentOne) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }

}
