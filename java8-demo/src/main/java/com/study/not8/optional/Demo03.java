package com.study.not8.optional;

import java.util.Optional;
import java.util.OptionalInt;
import java.util.Properties;

/**
 * @author tc
 * @date 2019/1/6
 */
public class Demo03 {

    public static void main(String[] args) {
        House house = new House();

        Properties properties = new Properties();
        properties.put("a", "300");
        properties.put("b", "200");

        String name = "a";

        System.out.println(duration(properties, name));

        name = "b";
        System.out.println(duration(properties, name));

    }

    public static int duration(Properties properties, String name) {
        return Optional.ofNullable(properties.getProperty(name))
            .flatMap(s -> OptionalUtility.stringToInt(s))
            .filter(i -> i > 200)
            .orElse(0);
    }

}

class House {

    private OptionalInt age;

    public OptionalInt getAge() {
        return age;
    }

    public void setAge(OptionalInt age) {
        this.age = age;
    }

}