package com.study.not8.optional;

import java.util.Optional;
import java.util.OptionalInt;

/**
 * @author tc
 * @date 2019/1/6
 */
public class OptionalUtility {

    public static OptionalInt stringToInt2(String s){
        try {
            int i = Integer.parseInt(s);
            return OptionalInt.of(i);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return OptionalInt.empty();
    }

    public static Optional<Integer> stringToInt(String s){
        try {
            int i = Integer.parseInt(s);
            return Optional.of(i);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

}
