package com.study.first;

import org.yaml.snakeyaml.Yaml;

import java.io.*;

/**
 * @author tc
 * @version 1.0
 * @date 2018/12/14 下午3:39
 */
public class LoadDemo {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("yaml-demo/src/main/resources/template/workflow-demo.wf.yaml");

        Yaml yaml = new Yaml();
        //StringBuilder result = new StringBuilder();
        //
        //try (BufferedReader br = new BufferedReader(new FileReader(file))) {
        //    String s;
        //    while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
        //        result.append(System.lineSeparator() + s);
        //    }
        //} catch (IOException e) {
        //    e.printStackTrace();
        //}


        Object result = yaml.load(new FileInputStream(file));
        System.out.println(result);

    }

}
