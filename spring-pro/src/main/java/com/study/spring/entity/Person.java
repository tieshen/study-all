package com.study.spring.entity;

/**
 * @author tc
 * @date 2018/12/25
 */
public class Person {

    private  String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
            "name='" + name + '\'' +
            '}';
    }

}
