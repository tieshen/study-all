package com.study.spring.conf;

import com.alibaba.nacos.api.annotation.NacosProperties;
import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import javax.annotation.PostConstruct;

/**
 * @author tc
 * @date 2019/1/31
 */
@Configuration
@Order(Integer.MIN_VALUE + 100)
@EnableNacosConfig(globalProperties = @NacosProperties(serverAddr = "nacos.middleware.test.gdapi.net:8848"))
@NacosPropertySource(dataId = "gd-test-tc.properties", groupId = "gd-test", autoRefreshed = true)
public class NacosConfig {

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

}
