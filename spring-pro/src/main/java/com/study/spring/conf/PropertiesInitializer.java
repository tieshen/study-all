package com.study.spring.conf;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @author tc
 * @date 2019/1/31
 */
@Configuration
public class PropertiesInitializer implements ApplicationContextInitializer {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        Object myProcess = applicationContext.getBeanFactory().getBean("myProcess");
        try {
            Method loadProperties = myProcess.getClass().getMethod("loadProperties", Properties.class);
            ReflectionUtils.invokeMethod(loadProperties, myProcess);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
