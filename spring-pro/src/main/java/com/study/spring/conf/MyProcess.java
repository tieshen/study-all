package com.study.spring.conf;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.study.spring.service.DemoClass;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringValueResolver;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Properties;

/**
 * @author tc
 * @date 2019/1/31
 */
@Configuration
@Order(Integer.MIN_VALUE + 5)
public class MyProcess extends PropertyPlaceholderConfigurer {

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props)
        throws BeansException {
        props.forEach((x, y) -> {
            System.out.format("key %s value %s ", x, y);
        });
    }

    @Override
    protected void loadProperties(Properties props) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("spring.file-service.sdk", Boolean.toString(false));
        properties.setProperty("spring.file-service.rpc.model", "hsf");
        properties.setProperty("nacos.config.server-addr", "nacos.middleware.test.gdapi.net:8848");
        CollectionUtils.mergePropertiesIntoMap(properties, props);
        super.loadProperties(props);
    }

    @Bean
    public DemoClass demoClass(){
        return new DemoClass();
    }

}
