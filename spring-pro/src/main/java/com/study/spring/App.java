package com.study.spring;

import com.study.spring.conf.MyProcess;
import com.study.spring.conf.NacosConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author tc
 * @date 2018/12/25
 */
public class App {

    public static void main(String[] args) throws IOException {
        //AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.study.spring");
        //context.getBeansOfType(Person.class).values().forEach(System.out::println);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        //context.register(MyProcess.class);
        //
        //context.register(NacosConfig.class);

        context.scan("com.study.spring.conf");
        //context.scan("com.gongdao.yuncourt");

        //context.getBeansOfType(Person.class).values().forEach(System.out::println);

        context.refresh();

        System.out.println("start print bean name");

        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
        //context.close();
        System.in.read();
    }

}
