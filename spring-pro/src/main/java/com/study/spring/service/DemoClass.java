package com.study.spring.service;

import com.alibaba.nacos.api.config.annotation.NacosValue;

import javax.annotation.PostConstruct;

/**
 * @author tc
 * @date 2019/1/31
 */
public class DemoClass {

    @NacosValue("${spring.file-service.clock:false}")
    private String fileService;

    @PostConstruct
    public void init(){
        System.out.println("fileService" + fileService);
    }


}
