package com.study.spring.service;

import com.study.spring.annotation.Api;

/**
 * @author tc
 * @date 2018/12/25
 */
@Api(version = "1.0.0")
public class RegistrarDemoImpl implements RegistrarDemo {
}
