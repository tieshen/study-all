package com.study.spring.core;

import com.study.spring.annotation.MyBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tc
 * @date 2018/12/25
 */
@Configuration
public class SpringRpcAutoConfiguration  {

    @Bean
    public MyBeanPostProcessor myBeanPostProcessor(){
        return new MyBeanPostProcessor();
    }

}
