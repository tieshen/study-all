package com.study.spring.annotation;

import com.study.spring.entity.Person;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author tc
 * @date 2018/12/25
 */
public class MyImportBeanDefinitionRegistrar  implements ImportBeanDefinitionRegistrar, EnvironmentAware,
    ResourceLoaderAware, BeanClassLoaderAware {

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {

    }

    @Override
    public void setEnvironment(Environment environment) {

    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {

    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        System.out.println("className : " + importingClassMetadata.getClassName());
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(Person.class);
        builder.addPropertyValue("name", "tieshen2");
        registry.registerBeanDefinition("person2", builder.getBeanDefinition());
    }

}
