package com.study.spring.annotation;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author tc
 * @date 2018/12/25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@Import(MyImportBeanDefinitionRegistrar.class)
public @interface Api {

    String version() default "";

}
