package com.study.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tc
 * @version 1.0
 * @date 2018/12/14 下午8:16
 */
public final class TimeUtils {

    private static final int PLUS = 1;

    private static final int MINUS = 2;

    public static String getTimeAroundDays(int hours) {
        return getTimeAroundDays(PLUS, hours);
    }

    public static String getTimeAroundDays(int mode, int hours) {
        String result;
        switch (mode) {
            case PLUS:
                result = getTimePlusDays(hours);
                break;
            case MINUS:
                result = getTimeMinusDays(hours);
                break;
            default:
                result = "";
        }

        return result;
    }

    public static String getTimeAroundHours(int hours) {
        return getTimeAroundHours(PLUS, hours);
    }

    public static String getTimeAroundHours(int mode, int hours) {
        String result;
        switch (mode) {
            case PLUS:
                result = getTimePlusHours(hours);
                break;
            case MINUS:
                result = getTimeMinusHours(hours);
                break;
            default:
                result = "";
        }

        return result;
    }

    private static String getTimePlusHours(int hours) {
        return ZonedDateTime.now().plusHours(hours).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    private static String getTimeMinusHours(int hours) {
        return ZonedDateTime.now().minusHours(hours).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    private static String getTimePlusDays(int days) {
        return ZonedDateTime.now().plusDays(days).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    private static String getTimeMinusDays(int days) {
        return ZonedDateTime.now().minusDays(days).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    public static void main(String[] args) {
        System.out.println(getTimeAroundHours(1));
        System.out.println(getTimeAroundDays(1));
        System.out.println(getTimeAroundDays(MINUS, 235));
    }

}
