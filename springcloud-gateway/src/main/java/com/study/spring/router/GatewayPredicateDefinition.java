package com.study.spring.router;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author tc
 * @date 2018/12/22
 */
public class GatewayPredicateDefinition {

    private String name;

    private Map<String, String> args = new LinkedHashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getArgs() {
        return args;
    }

    public void setArgs(Map<String, String> args) {
        this.args = args;
    }

}
