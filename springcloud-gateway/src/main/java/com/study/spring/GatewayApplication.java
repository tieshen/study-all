package com.study.spring;

import com.study.spring.filter.CustomGatewayFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

/**
 * Hello world!
 */
@SpringBootApplication
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class);
    }

    //@Bean
    //public RouteLocator myRoutes(RouteLocatorBuilder builder) {
    //    return builder
    //        .routes()
    //        .route(p -> p
    //            .path("/firstDemo")
    //            .filters(f -> f
    //                .addRequestHeader("from", "gateway")
    //            )
    //            .uri("http://127.0.0.1:8999")
    //            .id("firstDemo")
    //        ).build();
    //}

    //@Bean
    //public RouteLocator myRoutes(RouteLocatorBuilder builder) {
    //    return builder
    //        .routes()
    //        .route("header_router", r -> r.header("X-Request-Id", "tieshen")
    //        .uri("http://").id("headerDemo"))
    //        .build();
    //}

    @Bean
    public RouteLocator myRoutes2(RouteLocatorBuilder builder) {
        return builder
            .routes()
            .route(r -> r.path("/test")
                .uri("http://127.0.0.1:8999")
                .order(0)
                .id("custom_filter")
            ).build();

    }

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        return builder
            .routes()
            .route(r -> r.path("/*")
                .uri("http://127.0.0.1:8848/v1/ns/instances")
                .order(0)
                .id("demo")
            ).build();

    }

}
