package com.study.spring.filter;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.utils.ReferenceConfigCache;
import com.alibaba.dubbo.rpc.service.GenericService;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

/**
 * @author tc
 * @date 2018/12/22
 */
//@Component
public class MyDubboGenericFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println(exchange.getRequest().getQueryParams());

        ApplicationConfig application = new ApplicationConfig();
        application.setName("dubbo-generic-demo");

        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("nacos://127.0.0.1:8848");

        // 引用远程服务
        // 该实例很重量，里面封装了所有与注册中心及服务提供方连接，请缓存
        ReferenceConfig<GenericService> reference = new ReferenceConfig<>();
        // 弱类型接口名
        reference.setInterface("com.dubbo.nacos.service.DubboNacousDemoService");
        reference.setVersion("2.0.0");
        // 声明为泛化接口
        reference.setGeneric(true);
        reference.setRegistry(registry);
        reference.setApplication(application);

        ReferenceConfigCache cache = ReferenceConfigCache.getCache();

        // 用com.alibaba.dubbo.rpc.service.GenericService可以替代所有接口引用
        GenericService genericService = cache.get(reference);

        // 基本类型以及Date,List,Map等不需要转换，直接调用
        Object result = genericService.$invoke("sayHello", new String[] {"java.lang.String"}, new Object[] {"world"});

        String token = exchange.getRequest().getQueryParams().getFirst("token");
        System.out.println("token : " + token);
        Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
        URI uri = route.getUri();

        //ServerHttpResponse serverHttpResponse = exchange.getResponse();
        //DataBufferFactory dataBufferFactory = serverHttpResponse.bufferFactory();
        //ServerHttpResponseDecorator responseDecorator = new ServerHttpResponseDecorator(serverHttpResponse) {
        //
        //    @Override
        //    public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
        //        if (body instanceof Flux) {
        //            Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>)body;
        //            return super.writeWith(fluxBody.map(dataBuffer -> {
        //                // probably should reuse buffers
        //                byte[] content = null;
        //                dataBuffer.read(content);
        //                ////释放掉内存
        //                DataBufferUtils.release(dataBuffer);
        //                String str = null;
        //                if (result instanceof String) {
        //                    str = (String)result;
        //                    System.out.println("result : " + str);
        //                }
        //                return dataBufferFactory.wrap(str.getBytes());
        //            }));
        //        }
        //
        //        return super.writeWith(body);
        //    }
        //
        //};

        if (uri.getScheme().contains("nacos")) {
            exchange.getResponse().writeAndFlushWith(new Publisher<Publisher<? extends DataBuffer>>() {
                @Override
                public void subscribe(Subscriber<? super Publisher<? extends DataBuffer>> body) {
                    if (body instanceof Flux) {
                        Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>)body;

                    }
                }
            });
            return exchange.getResponse().setComplete();
        }

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -400;
    }

}
