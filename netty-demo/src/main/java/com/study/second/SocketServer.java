package com.study.second;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * HTTP
 * @author tc
 * @version 1.0
 * @date 2018/12/14 上午10:03
 */
public class SocketServer {

    public static void main(String[] args) {
        // 只是获取连接
        EventLoopGroup sourceGroup = new NioEventLoopGroup();  // handle
        EventLoopGroup workGroup = new NioEventLoopGroup();    // childHandler
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap
                .group(sourceGroup, workGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new SocketServerInitializer());

            ChannelFuture channelFuture = serverBootstrap.bind(8999).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            sourceGroup.shutdownGracefully();
            workGroup.shutdownGracefully();
        }
    }

}
