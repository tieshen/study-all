package com.study.first;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.net.URI;

/**
 * @author tc
 * @version 1.0
 * @date 2018/12/14 上午10:17
 */
public class TestServerHttpHandler extends SimpleChannelInboundHandler<HttpObject> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {
        System.out.println(msg.getClass());

        System.out.println("remote address: " + ctx.channel().remoteAddress());

        // 请求读取
        if (msg instanceof HttpRequest) {
            HttpRequest httpRequest = (HttpRequest)msg;

            System.out.println("请求方法名：" + httpRequest.method().name());

            URI uri = new URI(httpRequest.uri());

            // uri change to dubbo

            if ("favicon.ico".equals(uri.getPath())) {
                System.out.println("请求favicon.ico");
                return;
            }

            System.out.println("请求方法名：" + httpRequest.method().name());

            ByteBuf content = Unpooled.copiedBuffer("Hello World", CharsetUtil.UTF_8);
            FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
            response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain");
            response.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());

            ctx.writeAndFlush(response);
        }
    }

    // 重写基类方法学习

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // 活跃状态
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.channelActive(ctx);
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        // 新通道出现
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.handlerAdded(ctx);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.handlerRemoved(ctx);
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        // 注册
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.channelRegistered(ctx);
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        // 去除注册
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.channelUnregistered(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // 不活跃状态
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
        super.channelInactive(ctx);
    }
}
