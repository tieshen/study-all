package com.study.store.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.StringUtils;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author tc
 * @date 2018/12/29
 */
public class S3Service {

    private static final String accessKey = "4DREESNMVP23JILBKQ2I";
    private static final String secretKey = "Yagrc3HyglPZrCHIq71z7X0V";

    private static final String endpointUrl = "http://s3.gdapi.net";
    //private static final String endpointUrl = "http://s3i.gdapi.net";

    private static final String PRIVATE_BUCKET = "ceph_test";

    private static final String PUBLIC_BUCKET = "ceph_test_public";

    static AmazonS3 conn;

    static {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        conn = new AmazonS3Client(credentials);
        conn.setEndpoint(endpointUrl);
    }

    public static void main(String[] args) {
        //ByteArrayInputStream input = new ByteArrayInputStream("Hello World!".getBytes());
        //PutObjectResult putObjectResult = conn.putObject(PUBLIC_BUCKET, "first-file.txt", input, new ObjectMetadata
        // ());
        //System.out.println(putObjectResult.getVersionId());
        //System.out.println(putObjectResult.getETag());
        //System.out.println(putObjectResult.getContentMd5());

        // http://s3i.gdapi.net//ceph_test/hello.txt?
        // X-Amz-Algorithm=AWS4-HMAC-SHA256&
        // X-Amz-Date=20190102T024500Z&
        // X-Amz-SignedHeaders=host&
        // X-Amz-Expires=899&
        // X-Amz-Credential=4DREESNMVP23JILBKQ2I%2F20190102%2Fus-east-1%2Fs3%2Faws4_request&
        // X-Amz-Signature=5fe029a266b072f48a2c52516ad0dd293937a2e27ad4990aaeb1fb53da68e270
        //GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(PRIVATE_BUCKET, "hello.txt");
        //System.out.println(request.getContentMd5());
        //System.out.println(request.getKey());
        //System.out.println(request.getVersionId());
        //System.out.println(conn.generatePresignedUrl(request));

        ObjectListing objects = conn.listObjects(PUBLIC_BUCKET);
        do {
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                System.out.println(objectSummary.getKey() + "\t" +
                    objectSummary.getSize() + "\t" +
                    StringUtils.fromDate(objectSummary.getLastModified()));
            }
            objects = conn.listNextBatchOfObjects(objects);
        } while (objects.isTruncated());
        //
        //GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(PUBLIC_BUCKET, "first-file.txt");
        ////System.out.println(request.getContentMd5() + "\t" + StringUtils.fromDate(request.getExpiration()));
        //System.out.println(conn.generatePresignedUrl(request));

        int i;
        StringBuilder sb = new StringBuilder();
        S3Object object = conn.getObject(PUBLIC_BUCKET, "first-file.txt");

        try (S3ObjectInputStream objectContent = object.getObjectContent();
             FilterInputStream fis = new BufferedInputStream(objectContent);) {
            while ((i = fis.read()) != -1) {
                sb.append((char)i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static List<Bucket> listBuckets() {
        List<Bucket> buckets = conn.listBuckets();
        buckets.forEach(x -> System.out.println(x.getName()));
        return buckets;
    }

}
