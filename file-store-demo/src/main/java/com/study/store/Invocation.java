package com.study.store;

import com.amazonaws.services.s3.AmazonS3;

/**
 * 存储上下文
 *
 * @author tc
 * @date 2019/1/2
 */
public class Invocation {

    private AmazonS3 client;

    private FileObject object;

    private FileStoreMetadata metadata;

    public AmazonS3 getClient() {
        return client;
    }

    public void setClient(AmazonS3 client) {
        this.client = client;
    }

    public FileObject getObject() {
        return object;
    }

    public void setObject(FileObject object) {
        this.object = object;
    }

    public FileStoreMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(FileStoreMetadata metadata) {
        this.metadata = metadata;
    }

}
