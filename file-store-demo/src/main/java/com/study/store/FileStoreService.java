package com.study.store;

/**
 * @author tc
 * @date 2019/1/2
 */
public interface FileStoreService {

    /**
     * 文件上传
     *
     * @param invocation
     * @return
     */
    FileStoreResult upload(Invocation invocation);

    /**
     * 文件下载
     *
     * @param invocation
     * @return
     */
    FileStoreResult download(Invocation invocation);

    /**
     * url
     *
     * @param invocation
     * @return
     */
    FileStoreResult url(Invocation invocation);

    /**
     * 转移
     *
     * @param invocation
     * @return
     */
    FileStoreResult transfer(Invocation invocation);

    /**
     * 删除
     *
     * @param invocation
     * @return
     */
    FileStoreResult delete(Invocation invocation);

}
