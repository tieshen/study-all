package com.study.store;

import java.util.List;

/**
 * 存储元对象
 *
 * @author tc
 * @date 2019/1/2
 */
public class FileStoreMetadata {

    /**
     * 应用信息
     */
    private AppObject appObject;

    /**
     * 存储中心的accessKey
     */
    private String accessKey;

    /**
     * 存储中心的secretKey
     */
    private String secretKey;

    /**
     * 存储中心的endpointUrl
     */
    private String endpointUrl;

    /**
     * 存储中心的bucket
     */
    private List<String> bucketNames;

    /**
     * 激活的bucketName
     * <p>
     * 如果没有携带bucketName，会从{@link AppObject#defaultBucketName} 获取
     * </p>
     */
    private String activeBucketName;

    public AppObject getAppObject() {
        return appObject;
    }

    public void setAppObject(AppObject appObject) {
        this.appObject = appObject;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    public void setEndpointUrl(String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }

    public List<String> getBucketNames() {
        return bucketNames;
    }

    public void setBucketNames(List<String> bucketNames) {
        this.bucketNames = bucketNames;
    }

    public String getActiveBucketName() {
        return activeBucketName;
    }

    public void setActiveBucketName(String activeBucketName) {
        this.activeBucketName = activeBucketName;
    }

}
