package com.study.store;

import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.InputStream;

/**
 * 存储结果
 *
 * @author tc
 * @date 2019/1/2
 */
public class FileStoreResult {

    /**
     * 结果真假
     */
    private boolean result;

    /**
     * 对象元数据—头啊啥的
     */
    private ObjectMetadata metadata;

    /**
     * eTag
     */
    private String eTag;

    /**
     * 供下载的URL
     */
    private String url;

    /**
     * 文件信息
     */
    private FileObject object;

    /**
     * 输出的流
     */
    private InputStream inputStream;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ObjectMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ObjectMetadata metadata) {
        this.metadata = metadata;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FileObject getObject() {
        return object;
    }

    public void setObject(FileObject object) {
        this.object = object;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

}
