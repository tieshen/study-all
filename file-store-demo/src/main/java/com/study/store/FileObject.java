package com.study.store;

import java.io.File;
import java.io.InputStream;

/**
 * 文件对象
 *
 * @author tc
 * @date 2019/1/2
 */
public class FileObject {

    /**
     * 文件名称 上传时的名称
     */
    private String filename;

    /**
     * 文件大小
     */
    private long fileSize;

    /**
     * 存储中间件中的资源ID（文件操作的名称）
     */
    private String resourceId;

    /**
     * 文件对象 供文件上传使用
     */
    private File file;

    /**
     * 文件流
     */
    private InputStream inputStream;

    /**
     * 加密后的文件流
     */
    private InputStream encryptInputStream;

    /**
     * 是否加密 默认不加密
     * <p>
     * 如果 {@link AppObject#isEncrypt} 是true，此参数无效，所有的文件都需要加密
     * <br> 如果要指定文件加密或解密，{@link AppObject#isEncrypt} 必须设置成false
     * </p>
     */
    private boolean isEncrypt = false;

    /**
     * path + {@link FileObject#resourceId}
     */
    private String key;

    /**
     * 是否私有访问权限 私有才能创建有效期访问的URL
     */
    private boolean isPrivate;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public InputStream getEncryptInputStream() {
        return encryptInputStream;
    }

    public void setEncryptInputStream(InputStream encryptInputStream) {
        this.encryptInputStream = encryptInputStream;
    }

    public boolean isEncrypt() {
        return isEncrypt;
    }

    public void setEncrypt(boolean encrypt) {
        isEncrypt = encrypt;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

}
