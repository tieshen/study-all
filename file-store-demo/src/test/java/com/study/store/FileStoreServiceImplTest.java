package com.study.store;

import com.study.store.s3.S3Help;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.UUID;

/**
 * @author tc
 * @date 2019/1/3
 */
public class FileStoreServiceImplTest {

    private FileStoreService fileStoreService;

    @Before
    public void init() {
        fileStoreService = new FileStoreServiceImpl();
    }

    @Test
    public void upload() throws FileNotFoundException {

        Invocation invocation = new Invocation();
        FileStoreResult upload;
        AppObject appObject = new AppObject();
        appObject.setAppCode("lassen");

        FileStoreMetadata metadata = new FileStoreMetadata();

        FileObject fileObject = new FileObject();
        fileObject.setEncrypt(false);
        String s = UUID.randomUUID().toString();
        fileObject.setResourceId(s);
        fileObject.setKey(appObject.getAppCode() + "/" + s);
        fileObject.setFile(new File(
            "/Users/tc/Documents/workspace/gitee/study-all/file-store-demo/src/main/resources/service.test.txt"));

        invocation.setMetadata(metadata);
        invocation.setObject(fileObject);

        // 上传S3 流的形式 私有bucket
        //metadata.setAccessKey("4DREESNMVP23JILBKQ2I");
        //metadata.setSecretKey("Yagrc3HyglPZrCHIq71z7X0V");
        //metadata.setEndpointUrl("http://s3.gdapi.net");
        //metadata.setActiveBucketName("ceph_test");
        //invocation.setClient(S3Help.createClient(metadata.getAccessKey(), metadata.getSecretKey(), metadata
        // .getEndpointUrl()));

        //upload = fileStoreService.upload(invocation);

        //upload = fileStoreService.upload(invocation);

        // 上传OSS 流的形式 私有bucket
        metadata.setAccessKey("LTAIfbnbWID59wvR");
        metadata.setSecretKey("85cNWjjb9vXKIKUZxJHqN89fwGAZbY");
        metadata.setEndpointUrl("oss-cn-beijing.aliyuncs.com");
        metadata.setActiveBucketName("gongdao-test");
        invocation.setClient(
            S3Help.createClient(metadata.getAccessKey(), metadata.getSecretKey(), metadata.getEndpointUrl()));

        //upload = fileStoreService.upload(invocation);

        fileObject.setFile(null);
        fileObject.setInputStream(new FileInputStream(new File(
            "/Users/tc/Documents/workspace/gitee/study-all/file-store-demo/src/main/resources/20180730233201.png")));

        upload = fileStoreService.upload(invocation);

    }

    @Test
    public void download() {
        Invocation invocation = new Invocation();
        FileStoreResult upload;
        AppObject appObject = new AppObject();
        appObject.setAppCode("lassen");

        FileStoreMetadata metadata = new FileStoreMetadata();

        FileObject fileObject = new FileObject();
        fileObject.setEncrypt(false);
        String s = "74d4fac9-2a28-49f5-8369-da1d382c453b";
        fileObject.setResourceId(s);
        fileObject.setKey(appObject.getAppCode() + "/" + s);

        invocation.setMetadata(metadata);
        invocation.setObject(fileObject);

        metadata.setAccessKey("4DREESNMVP23JILBKQ2I");
        metadata.setSecretKey("Yagrc3HyglPZrCHIq71z7X0V");
        metadata.setEndpointUrl("http://s3.gdapi.net");
        metadata.setActiveBucketName("ceph_test");
        invocation.setClient(S3Help.createClient(metadata.getAccessKey(), metadata.getSecretKey(), metadata
            .getEndpointUrl()));

         upload = fileStoreService.download(invocation);
    }

    @Test
    public void url() {
        Invocation invocation = new Invocation();
        FileStoreResult upload;
        AppObject appObject = new AppObject();
        appObject.setAppCode("lassen");

        FileStoreMetadata metadata = new FileStoreMetadata();

        FileObject fileObject = new FileObject();
        fileObject.setEncrypt(false);
        String s = "74d4fac9-2a28-49f5-8369-da1d382c453b";
        fileObject.setResourceId(s);
        fileObject.setKey(appObject.getAppCode() + "/" + s);

        invocation.setMetadata(metadata);
        invocation.setObject(fileObject);

        metadata.setAccessKey("LTAIfbnbWID59wvR");
        metadata.setSecretKey("85cNWjjb9vXKIKUZxJHqN89fwGAZbY");
        metadata.setEndpointUrl("oss-cn-beijing.aliyuncs.com");
        metadata.setActiveBucketName("gongdao-test");
        invocation.setClient(
            S3Help.createClient(metadata.getAccessKey(), metadata.getSecretKey(), metadata.getEndpointUrl()));

        System.out.println(fileStoreService.url(invocation).getUrl());
    }

    @Test
    public void transfer() {

    }

    @Test
    public void delete() {
    }
}