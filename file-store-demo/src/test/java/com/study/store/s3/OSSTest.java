package com.study.store.s3;

import com.aliyun.oss.HttpMethod;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tc
 * @date 2019/1/7
 */
public class OSSTest {

    protected static final String bucketName = "gongdao-test-public";
    protected static OSSClient ossClient = null;

    protected static AmazonS3 conn = null;

    static String FILE_KEY = "lassen/demo.txt";

    private static final String accessKey = "LTAIfbnbWID59wvR";
    private static final String secretKey = "85cNWjjb9vXKIKUZxJHqN89fwGAZbY";
    private static final String endpointUrl = "oss-cn-beijing.aliyuncs.com";
    //private static final String endpointUrl = "http://s3i.gdapi.net";

    private static final String PRIVATE_BUCKET = "ceph_test";
    private static final String PUBLIC_BUCKET = "ceph_test_public";

    private static final String accessKey_s3 = "4DREESNMVP23JILBKQ2I";
    private static final String secretKey_s3 = "Yagrc3HyglPZrCHIq71z7X0V";
    private static final String endpointUrl_s3 = "http://s3.gdapi.net";

    @Before
    public void init() {
        long startOss = System.currentTimeMillis();
        // 创建OSSClient实例。
        ossClient = new OSSClient(endpointUrl, accessKey, secretKey);

        System.out.println(System.currentTimeMillis() - startOss);

        long startS3 = System.currentTimeMillis();
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey_s3, secretKey_s3);
        AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(
            endpointUrl_s3, Regions.CN_NORTH_1.getName());
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withEndpointConfiguration(endpointConfiguration)
            .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
        conn = s3Client;

        System.out.println(System.currentTimeMillis() - startS3);
    }

    @After
    public void end() {
        ossClient.shutdown();

        conn.shutdown();
    }

    @Test
    public void getSignUrl() throws Exception {
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, FILE_KEY);
        // HttpMethod为PUT。
        generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
        //// 添加用户自定义元信息。
        //  generatePresignedUrlRequest.addUserMetadata("author", "tie");
        // 添加Content-Type。
        generatePresignedUrlRequest.setContentType("application/octet-stream");
        //// 设置URL过期时间为1小时。
        Date expiration = new Date(new Date().getTime() + 3600 * 1000);
        generatePresignedUrlRequest.setExpiration(expiration);
        // 生成签名URL。
        URL url = ossClient.generatePresignedUrl(generatePresignedUrlRequest);

        System.out.println(url.toString());

        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setDoOutput(true);

        urlConnection.setRequestProperty("Content-Type", "application/octet-stream");
        urlConnection.setRequestMethod("PUT");

        // 使用签名URL发送请求。
        File f = new File(
            "/Users/tc/Documents/workspace/gitee/study-all/file-store-demo/src/main/resources/demo-3.txt");
        FileInputStream fin = new FileInputStream(f);
        // 添加PutObject请求头。
        //Map<String, String> customHeaders = new HashMap<String, String>();
        //customHeaders.put("Content-Type", "application/octet-stream");
        //customHeaders.put("x-oss-meta-author", "tie");

        OutputStream outputStream = urlConnection.getOutputStream();
        byte data[] = new byte[100];
        int size = -1;
        while (-1 != (size = fin.read(data))) {
            outputStream.write(data, 0, size);
        }
        outputStream.flush();

        if (urlConnection.getResponseCode() >= 300) {
            throw new Exception("HTTP Request is not success, Response code is " + urlConnection.getResponseCode());
        }

        //接收响应流
        InputStream inputStream2 = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();
        String tempLine = null;

        inputStream2 = urlConnection.getInputStream();
        inputStreamReader = new InputStreamReader(inputStream2);
        reader = new BufferedReader(inputStreamReader);

        while ((tempLine = reader.readLine()) != null) {
            resultBuffer.append(tempLine);
        }
        System.out.println(resultBuffer.toString());

        outputStream.close();
        fin.close();
    }


}
