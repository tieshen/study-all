package com.study.store.s3;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author tc
 * @date 2019/1/2
 */
public class S3HelpTest {

    static AmazonS3 conn;

    private static final String accessKey = "4DREESNMVP23JILBKQ2I";
    private static final String secretKey = "Yagrc3HyglPZrCHIq71z7X0V";
    private static final String endpointUrl = "http://s3.gdapi.net";
    //private static final String endpointUrl = "http://s3i.gdapi.net";

    private static final String PRIVATE_BUCKET = "ceph_test";
    private static final String PUBLIC_BUCKET = "ceph_test_public";

    @Before
    public void init() {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(
            endpointUrl, Regions.CN_NORTH_1.getName());
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withEndpointConfiguration(endpointConfiguration)
            .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
        conn = s3Client;
    }

    @Test
    public void putObject() {
        String key = "lassen/demo.txt";
        ByteArrayInputStream input = new ByteArrayInputStream("I am Demo!".getBytes());
        PutObjectResult putObjectResult = S3Help.putObject(conn, PUBLIC_BUCKET, key, input);
        System.out.println(putObjectResult.getETag() + "\t" + putObjectResult.getContentMd5());
    }

    @Test
    public void putObject1() {
        String key = "lassen/demo-2.txt";
        ByteArrayInputStream input = new ByteArrayInputStream("I am Demo 2!".getBytes());
        PutObjectResult putObjectResult = S3Help.putObject(conn, PUBLIC_BUCKET, key, input,
            CannedAccessControlList.Private);
        System.out.println(putObjectResult.getETag() + "\t" + putObjectResult.getContentMd5());
    }

    @Test
    public void putObject2() {
        String key = "lassen/demo-3.txt";
        File file = new File(
            "/Users/tc/Documents/workspace/gitee/study-all/file-store-demo/src/main/resources/demo-3.txt");
        //String s = S3Help.putObject(conn, PRIVATE_BUCKET, key, file,
        //    CannedAccessControlList.Private);
        //System.out.println(s);
    }

    @Test
    public void delete() {
    }

    @Test
    public void readString() {
        //String key = "lassen/a6066633-94d3-40ff-b493-bc2d4f226b12";
        //String s = S3Help.readString(conn, PRIVATE_BUCKET, key);
        //
        //Assert.assertNotNull(s);
        //
        //System.out.println(s);

        String key2 = "lassen/demo-url.txt";
        String s2 = S3Help.readString(conn, PUBLIC_BUCKET, key2);

        Assert.assertNotNull(s2);

        System.out.println(s2);
    }

    @Test
    public void readStream() {

        List<Bucket> buckets = S3Help.getBuckets(conn);

        for (Bucket bucket : buckets) {

        }

    }

    @Test
    public void isExist() {
        // not exist
        String key = "first-file1.txt";

        boolean exist = S3Help.isExist(conn, PUBLIC_BUCKET, key);

        System.out.println(exist == true ? "success" : "false");
    }

    @Test
    public void listObject() {
        ObjectListing objectListing = S3Help.listObject(conn, PRIVATE_BUCKET);
        //jiuding/
        System.out.println(conn.doesObjectExist(PRIVATE_BUCKET, "katla/0af5a72f-e2ab-4fcb-8e41-3a9d21bb1918"));
    }

    @Test
    public void preSignedUrl() {
        String key1 = "lassen/demo-3.txt";

        System.out.println("Generating pre-signed URL.");
        java.util.Date expiration = new java.util.Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += 1000 * 60 * 60;
        expiration.setTime(milliSeconds);

        GeneratePresignedUrlRequest generatePresignedUrlRequest =
            new GeneratePresignedUrlRequest(PUBLIC_BUCKET, key1);
        generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
        generatePresignedUrlRequest.setExpiration(expiration);
        URL url = conn.generatePresignedUrl(generatePresignedUrlRequest);

        System.out.println(url.toString());
        //String url1 = S3Help.preSignedUrl(conn, PUBLIC_BUCKET, key1);
        //System.out.println(url1);
    }

    @Test
    public void getSignUrl2() throws Exception {
        //String uuid = UUID.randomUUID().toString();
        //
        //System.out.println(uuid);
        //
        //Date expiration = new Date(new Date().getTime() + 3600 * 1000);
        //GeneratePresignedUrlRequest
        //    generatePresignedUrlRequest = new GeneratePresignedUrlRequest(PUBLIC_BUCKET, "lassen/" + uuid + ".txt")
        //    .withMethod(HttpMethod.PUT).withContentType("application/octet-stream").withExpiration(expiration);
        // HttpMethod为PUT。
        //generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
        //// 添加用户自定义元信息。
        //  generatePresignedUrlRequest.addUserMetadata("author", "tie");
        // 添加Content-Type。
        //generatePresignedUrlRequest.setContentType("application/octet-stream");
        //// 设置URL过期时间为1小时。

        //generatePresignedUrlRequest.setExpiration(expiration);
        // 生成签名URL。
        //URL url = conn.generatePresignedUrl(generatePresignedUrlRequest);
        //String s = url.toString();
        //System.out.println(s);

        // katla/482789f2-0514-44b2-a061-89e151e177e6

        URL url2 = new URL("http://s3.gdapi.net/ceph_test_public/katla/0af5a72f-e2ab-4fcb-8e41-3a9d21bb1918?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20190122T061148Z&X-Amz-SignedHeaders=content-type%3Bhost&X-Amz-Expires=1799&X-Amz-Credential=4DREESNMVP23JILBKQ2I%2F20190122%2Fcn-north-1%2Fs3%2Faws4_request&X-Amz-Signature=0640333dacdb5de2f48668472744b4109f1605eef82240ee9ffc824a0b92e7bd");

        HttpURLConnection urlConnection = (HttpURLConnection)url2.openConnection();
        urlConnection.setDoOutput(true);

        urlConnection.setRequestProperty("Content-Type", "application/octet-stream");
        urlConnection.setRequestMethod("PUT");

        // 使用签名URL发送请求。
        File f = new File(
            "/Users/tc/Documents/workspace/gitee/study-all/file-store-demo/src/main/resources/demo-3.txt");
        FileInputStream fin = new FileInputStream(f);
        // 添加PutObject请求头。
        //Map<String, String> customHeaders = new HashMap<String, String>();
        //customHeaders.put("Content-Type", "application/octet-stream");
        //customHeaders.put("x-oss-meta-author", "tie");

        OutputStream outputStream = urlConnection.getOutputStream();
        byte data[] = new byte[100];
        int size = -1;
        while (-1 != (size = fin.read(data))) {
            outputStream.write(data, 0, size);
        }
        outputStream.flush();

        if (urlConnection.getResponseCode() >= 300) {
            throw new Exception("HTTP Request is not success, Response code is " + urlConnection.getResponseCode());
        }

        //接收响应流
        InputStream inputStream2 = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();
        String tempLine = null;

        inputStream2 = urlConnection.getInputStream();
        inputStreamReader = new InputStreamReader(inputStream2);
        reader = new BufferedReader(inputStreamReader);

        while ((tempLine = reader.readLine()) != null) {
            resultBuffer.append(tempLine);
        }
        System.out.println(resultBuffer.toString());

        outputStream.close();
        fin.close();
    }

    public static void main(String[] args) {
        String s
            = "http://s3.gdapi.net/ceph_test_public/katla/0af5a72f-e2ab-4fcb-8e41-3a9d21bb1918?X-Amz-Algorithm=AWS4"
            + "-HMAC-SHA256&X-Amz-Date=20190122T061148Z&X-Amz-SignedHeaders=content-type%3Bhost&X-Amz-Expires=1799&X"
            + "-Amz-Credential=4DREESNMVP23JILBKQ2I%2F20190122%2Fcn-north-1%2Fs3%2Faws4_request&X-Amz-Signature"
            + "=0640333dacdb5de2f48668472744b4109f1605eef82240ee9ffc824a0b92e7bd";
        System.out.println(s.length());
    }

}