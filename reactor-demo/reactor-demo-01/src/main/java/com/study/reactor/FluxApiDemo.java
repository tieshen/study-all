package com.study.reactor;

import com.study.util.Utils;
import reactor.core.publisher.Flux;

import java.util.Random;

/**
 * @author tc
 * @date 2018/12/27
 */
public class FluxApiDemo {

    public static void main(String[] args) {
        Random random = new Random();

        Flux.generate(() -> 0, (value, sink) -> {

            if (value == 3) {
                sink.complete();
            } else {
                sink.next("value = " + value);
            }

            return value + 1;
        }).subscribe(
            Utils::println,
            Utils::println,
            () -> {
                Utils.println("Subscription is complete!");
            });
    }

}
