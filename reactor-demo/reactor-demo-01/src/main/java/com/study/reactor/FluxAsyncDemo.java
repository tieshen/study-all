package com.study.reactor;

import com.study.util.Utils;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * 异步操作
 *
 * @author tc
 * @date 2018/12/27
 */
public class FluxAsyncDemo {

    public static void main(String[] args) throws InterruptedException {
        // 当前线程执行
        //Flux.range(0, 10)
        //    .publishOn(Schedulers.immediate())
        //    .subscribe(Utils::println);

        // 单线程异步执行
        //Flux.range(0, 10)
        //    .publishOn(Schedulers.single())
        //    .subscribe(Utils::println);

        // 弹性线程池异步执行
        //Flux.range(0, 10)
        //    .publishOn(Schedulers.elastic())
        //    .subscribe(Utils::println);

        // 异步并行执行
        Flux.range(0, 10)
            .publishOn(Schedulers.parallel())
            .subscribe(Utils::println);

        Thread.currentThread().join(1 * 1000L);
    }


}
