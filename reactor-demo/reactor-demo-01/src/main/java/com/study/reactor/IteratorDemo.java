package com.study.reactor;

import java.util.Iterator;
import java.util.List;

/**
 * @author tc
 * @date 2018/12/26
 */
public class IteratorDemo {

    public static void main(String[] args) {

        // 数据源
        List<Integer> values = List.of(1, 2, 3, 4, 5);

        // 迭代
        // 消费数据
        Iterator<Integer> iterator = values.iterator();
        // 这个过程向服务器请求数据
        while (iterator.hasNext()) {
            Integer value = iterator.next();  // 主动获取 拉的模式
            System.out.println(value);
        }

    }

}
