package com.study.reactor;

import com.study.util.Utils;
import reactor.core.publisher.Flux;

import java.util.Random;

/**
 * @author tc
 * @date 2018/12/27
 */
public class FluxLambdaDemo {

    public static void main(String[] args) {
        //Flux<Integer> flux = Flux.just(1, 2, 3);

        // 订阅并且处理（控制台输出）
        //flux.subscribe(Utils::println);

        Random random = new Random();

        Flux.just(1, 2, 3).map(value -> {

            // value == 3 抛出异常
            //if (value == 3) {
            //    throw new RuntimeException(" value nust be less than 3!");
            //}

            if (random.nextInt(4) == 3) {
                throw new RuntimeException(" value nust be less than 3!");
            }

            return value + 1;
        }).subscribe(
            Utils::println,   // onNext()
            Utils::println,    // onError()
            () -> {
                Utils.println("Subscription is complete!");
            }
        );
    }

}
