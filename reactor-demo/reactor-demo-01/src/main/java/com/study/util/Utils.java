package com.study.util;

/**
 * @author tc
 * @date 2018/12/27
 */
public class Utils {

    public static void println(Object message) {
        System.out.printf("[Thread : %s ] : %s \n", Thread.currentThread().getName(), String.valueOf(message));
    }

}
